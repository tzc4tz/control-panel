import Row from "@/components/atoms/Row";
import Col from "@/components/atoms/Col";
import Navbar from "@/components/organisms/navbar";
import Sidebar from "@/components/organisms/sidebar";

import type { LayoutProps } from "./interface";
import styles from "./layout.module.scss";

const Layout = ({ children }: LayoutProps) => {
  return (
    <Row className={styles["layout"]}>
      <Sidebar />
      <Col flex="auto">
        <Navbar />
        <main className={styles["layout--content"]}>{children}</main>
      </Col>
    </Row>
  );
};

export default Layout;
