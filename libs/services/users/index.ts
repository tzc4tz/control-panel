import API from "@/utils/statics/routes/api";

export const getUsers = async () => {
  return await fetch(`${API.ROOT}${API.USERS}`).then((res) => res.json());
};
