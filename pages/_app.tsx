import "@/styles/globals.scss";
import { useEffect } from "react";
import { QueryClient, QueryClientProvider } from "react-query";
import { setTheme } from "@/utils/helpers/global";
import AuthProvider from "@/components/hoc/authRoute";

import type { AppContext } from "next/app";
import type { AppPropsWithTheme } from "@/utils/interfaces";

const queryClient = new QueryClient();

const App = ({
  Component,
  pageProps,
  theme,
  permissions,
}: AppPropsWithTheme) => {
  useEffect(() => {
    if (theme && theme !== "default") setTheme(theme);
  }, [theme]);

  return (
    <QueryClientProvider client={queryClient}>
      <AuthProvider permissions={permissions}>
        <Component {...pageProps} />
      </AuthProvider>
    </QueryClientProvider>
  );
};

App.getInitialProps = async ({ ctx }: AppContext) => {
  const currentUserPermissions = [
    "list.create",
    "list.read",
    "list.update",
    "list.destroy",
    "home.read",
  ];
  const { req } = ctx;
  const themeIndex = req?.headers.cookie?.search("theme");
  const theme = req?.headers.cookie?.slice(themeIndex)?.split("theme=")?.[1];

  return { theme, permissions: currentUserPermissions };
};

export default App;
