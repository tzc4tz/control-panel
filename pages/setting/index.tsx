import SettingTemplate from "@/components/templates/setting";

const SettingPage = () => <SettingTemplate />;

export default SettingPage;
