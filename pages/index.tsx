import HomeTemplate from "@/components/templates/home";

const Home = () => <HomeTemplate />;

export default Home;
