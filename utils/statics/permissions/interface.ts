export type Permissions =
  | "list.create"
  | "list.read"
  | "list.update"
  | "list.destroy"
  | "setting.create"
  | "setting.read"
  | "setting.update"
  | "setting.destroy";

export interface RouteWithPerm {
  pathname: string;
  permission?: Permissions;
  publicRoute?: boolean;
}
