import WEB from "../routes/web";
import type { RouteWithPerm } from "./interface";

const routeWithPermissions: RouteWithPerm[] = [
  {
    pathname: WEB.HOME,
    publicRoute: true,
  },
  {
    pathname: WEB.LIST,
    permission: "list.read",
  },
  {
    pathname: WEB.SETTING,
    permission: "setting.read",
  },
];

export default routeWithPermissions;
