const API = {
    ROOT: 'https://jsonplaceholder.typicode.com',
    USERS: '/users'
}

export default API