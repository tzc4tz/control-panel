import type { AppProps } from "next/app";
import type { Themes } from "../helpers/theme";
import type { Permissions } from "../statics/permissions/interface";

export type AppPropsWithTheme = AppProps & {
  theme?: Themes;
  permissions?: Permissions[];
};

export type LinkTarget = "_self" | "_blank" | "_parent" | "_top";

export type PresetColorType =
  | "bg-color"
  | "black"
  | "white"
  | "simple-white"
  | "dark-matter"
  | "corn-silk"
  | "quick-silver"
  | "gray-matter"
  | "platinum"
  | "text-color"
  | "gray"
  | "secondary"
  | "primary"
  | "mari-gold"
  | "amethyst"
  | "jade"
  | "darkish-blue"
  | "transparent";
