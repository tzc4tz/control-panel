export type Themes = "default" | "alt" | "dark";

export type KeyColors =
  | "--primary"
  | "--secondary"
  | "--bg-color"
  | "--text-color";

type Pallets = Record<KeyColors, string | null>;

type ThemeOptions = Record<Themes, Pallets>;

const themeOptions: ThemeOptions = {
  default: {
    "--primary": "#0d72ff",
    "--secondary": "#e8422c",
    "--bg-color": "#fff",
    "--text-color": "#40454b",
  },
  alt: {
    "--primary": "#9b69c6",
    "--secondary": "#eda224",
    "--bg-color": "#fff",
    "--text-color": "#000",
  },
  dark: {
    "--primary": "#00ab66",
    "--secondary": "#0d72ff",
    "--bg-color": "#000",
    "--text-color": "#fff",
  },
};

export default themeOptions;
