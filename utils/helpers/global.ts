import Cookies from "js-cookie";
import themeOptions, { type KeyColors, type Themes } from "./theme";

import type { Permissions } from "../statics/permissions/interface";

export const setTheme = (theme: Themes) => {
  Cookies.set("theme", theme);
  const root = document.documentElement;

  if (root) {
    Object.keys(themeOptions[theme]).forEach((key) => {
      const color = themeOptions[theme][key as KeyColors];
      root.style.setProperty(key, color);
    });
  }
};

export const checkPermission = (
  permissions: Permissions | Permissions[],
  userPermissions?: Permissions[] | null
): boolean => {
  if (!userPermissions) return false;
  if (typeof permissions === "object") {
    const access = permissions.map((perm) => userPermissions.includes(perm));
    return access.every((value) => value === true);
  }
  return userPermissions.includes(permissions);
};
