import Row from "@/components/atoms/Row";
import Col from "@/components/atoms/Col";

import styles from "./home.module.scss";

const HomeTemplate = () => {
  return (
    <Row className={styles["home"]} wrap>
      <Col className="text-weight-700 text-6" span={24}>
        Simple Control Panel With Private Route & Custom Theme & Utility Classes
      </Col>
      <Col className="pt-2" span={24}>
        to try out the private route , check the {`"`}/setting{`"`} route
      </Col>
    </Row>
  );
};

export default HomeTemplate;
