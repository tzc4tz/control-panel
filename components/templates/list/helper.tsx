import Row from "@/components/atoms/Row";
import Col from "@/components/atoms/Col";

import styles from "./list.module.scss";

export const ListHeader = () => {
  return (
    <Col span={24}>
      <Row className={styles["list__header"]} justify="space-between" wrap>
        <Col span={4}>username</Col>
        <Col span={6}>name</Col>
        <Col span={2}>id</Col>
        <Col span={6}>email</Col>
        <Col span={6}>phone</Col>
      </Row>
    </Col>
  );
};
