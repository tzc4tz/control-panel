import { useQuery } from "react-query";
import { getUsers } from "@/libs/services/users";
import { ListHeader } from "./helper";
import Row from "@/components/atoms/Row";
import Col from "@/components/atoms/Col";

import type { UserList } from "@/libs/services/users/interface";
import styles from "./list.module.scss";

const ListTemplate = () => {
  const { data, error, isLoading } = useQuery<UserList[], unknown>(
    "users",
    getUsers
  );

  if (error) return <span>An error was occurred</span>;

  return (
    <Row className={styles["list"]} wrap>
      <ListHeader />
      {isLoading ? (
        "Loading"
      ) : (
        <Col span={24}>
          {data?.map(({ username, name, id, email, phone }) => (
            <Row
              key={id}
              className={styles["list__content"]}
              justify="space-between"
              wrap
            >
              <Col span={4}>{username}</Col>
              <Col span={6}>{name}</Col>
              <Col span={2}>{id}</Col>
              <Col span={6}>{email}</Col>
              <Col span={6}>{phone}</Col>
            </Row>
          ))}
        </Col>
      )}
    </Row>
  );
};

export default ListTemplate;
