import { type ReactNode, createContext } from "react";
import { useRouter } from "next/router";
import Layout from "@/layout";
import AccessDenied from "../molecules/accessDenied";
import routeWithPermissions from "@/utils/statics/permissions";

import type { Permissions } from "@/utils/statics/permissions/interface";

interface AuthProviderProps {
  children: ReactNode;
  permissions?: Permissions[];
}

interface AuthContextValue {
  permissions?: Permissions[] | null;
}

export const AuthContext = createContext<AuthContextValue>({
  permissions: null,
});
const { Provider } = AuthContext;

const AuthProvider = ({ children, permissions }: AuthProviderProps) => {
  const { pathname } = useRouter();

  const appRoute = routeWithPermissions.find(
    (route) => pathname === route.pathname
  );

  if (
    appRoute?.publicRoute ||
    (appRoute?.permission && permissions?.includes(appRoute?.permission))
  ) {
    return (
      <Provider value={{ permissions }}>
        <Layout>{children}</Layout>
      </Provider>
    );
  }

  return (
    <Layout>
      <AccessDenied />
    </Layout>
  );
};

export default AuthProvider;
