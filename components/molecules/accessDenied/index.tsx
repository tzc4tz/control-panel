import Row from "@/components/atoms/Row";
import Col from "@/components/atoms/Col";
import Button from "@/components/atoms/Button";

import WEB from "@/utils/statics/routes/web";
import styles from "./accessDenied.module.scss";

const AccessDenied = () => {
  return (
    <Row className={styles["noAccess"]} justify="center" wrap gutter={[16, 16]}>
      <Col className="text-center text-5 color-text-color" span={24}>
        You have not access to this page
      </Col>
      <Col>
        <Button href={WEB.HOME} ripple>
          return to home
        </Button>
      </Col>
    </Row>
  );
};

export default AccessDenied;
