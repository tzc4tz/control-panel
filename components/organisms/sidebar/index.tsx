import { useContext } from "react";
import { AuthContext } from "@/components/hoc/authRoute";
import { checkPermission } from "@/utils/helpers/global";
import Row from "@/components/atoms/Row";
import Col from "@/components/atoms/Col";
import Button from "@/components/atoms/Button";

import WEB from "@/utils/statics/routes/web";
import styles from "./sidebar.module.scss";

const Sidebar = () => {
  const { permissions } = useContext(AuthContext);

  return (
    <Col className={styles["sidebar"]} span={4}>
      <Row>
        <Button href={WEB.LIST}>List</Button>
      </Row>
      {checkPermission("setting.read", permissions) && (
        <Row>
          <Button href={WEB.SETTING}>Setting</Button>
        </Row>
      )}
    </Col>
  );
};

export default Sidebar;
