import { setTheme } from "@/utils/helpers/global";
import themeOptions, { type Themes } from "@/utils/helpers/theme";
import Row from "@/components/atoms/Row";
import Col from "@/components/atoms/Col";
import Button from "@/components/atoms/Button";

import styles from "./navbar.module.scss";

const Navbar = () => {
  const selectTheme = (theme: Themes) => setTheme(theme);

  return (
    <Row className={styles["navbar"]} justify="start" align="middle" gap={0}>
      <Col className="color-primary" flex="none">
        Themes :
      </Col>
      {Object.keys(themeOptions).map((key) => (
        <Col className={styles["navbar__theme"]} key={key} flex="none">
          <Button
            data-selector={key}
            onClick={() => selectTheme(key as Themes)}
            ripple
          >
            {key}
          </Button>
        </Col>
      ))}
    </Row>
  );
};

export default Navbar;
